use clap::Parser;

/// Map CSV to RDF with handlebars and Chowlk
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Template graph file from the Chowlk converter
    #[arg(short, long)]
    graph_file: String,

    /// CSV input file
    #[arg(short, long)]
    csv_file: String,
}

fn main() {
    let args = Args::parse();

    println!("Hello {}!", args.graph_file);
    println!("Hello {}!", args.csv_file);
    println!("Hello, world!");
}
